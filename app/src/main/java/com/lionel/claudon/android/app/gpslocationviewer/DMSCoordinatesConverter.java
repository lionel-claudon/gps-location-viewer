package com.lionel.claudon.android.app.gpslocationviewer;

/**
 * Created by lionel on 03/11/15.
 */
public class DMSCoordinatesConverter {
    public static DMSCoordinates convertToDMS(double latitude, double longitude) {
        long latD = (long) latitude;
        double latM = 60*(latitude-latD);
        double latS = 60*(latM - (long)latM);
        String latHemisphere = latD > 0 ? "N" : "S";

        long longD = (long) longitude;
        double longM = 60*(longitude-longD);
        double longS = 60*(longM - (long)longM);
        String longHemisphere = longD > 0 ? "E" : "W";

        return new DMSCoordinates(latHemisphere, latD, (long) latM, latS, longHemisphere, longD, (long) longM, longS);
    }
}

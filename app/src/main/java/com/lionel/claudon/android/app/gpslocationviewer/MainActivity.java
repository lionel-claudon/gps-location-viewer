package com.lionel.claudon.android.app.gpslocationviewer;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private static final int THIRTY_SECONDS = 1000 * 30;

    private Location lastKnownLocation;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Timer lastUpdateTimer;

    // Views
    private TextView lastUpdatedTextView;
    private TextView locationSourceTextView;
    private TextView locationAccuracyTextView;
    private TextView latitudeDDTextView;
    private TextView longitudeDDTextView;
    private TextView latitudeDMSTextView;
    private TextView longitudeDMSTextView;
    private TextView latitudeUTMTextView;
    private TextView longitudeUTMTextView;
    private TextView zoneUTMTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            lastUpdatedTextView = (TextView) findViewById(R.id.lastUpdatedValue);
            locationSourceTextView = (TextView) findViewById(R.id.sourceValue);
            locationAccuracyTextView = (TextView) findViewById(R.id.accuracyValue);
            latitudeDDTextView = (TextView) findViewById(R.id.latitudeDDValue);
            longitudeDDTextView = (TextView) findViewById(R.id.longitudeDDValue);
            latitudeDMSTextView = (TextView) findViewById(R.id.latitudeDMSValue);
            longitudeDMSTextView = (TextView) findViewById(R.id.longitudeDMSValue);
            latitudeUTMTextView = (TextView) findViewById(R.id.latitudeUTMValue);
            longitudeUTMTextView = (TextView) findViewById(R.id.longitudeUTMValue);
            zoneUTMTextView = (TextView) findViewById(R.id.zoneUTMValue);

            // Define a listener that responds to location updates
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    treatNewLocation(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
        } catch (SecurityException e) {
            Log.e(MainActivity.class.getSimpleName(), "Error while starting activity", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if(lastUpdateTimer != null) {
                lastUpdateTimer.cancel();
            }

            // Remove the listener you previously added
            if (locationManager != null) {
                locationManager.removeUpdates(locationListener);
            }
        } catch(SecurityException e) {
            Log.e(MainActivity.class.getSimpleName(), "Error while stopping activity", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            // Acquire a reference to the system Location Manager
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if(lastKnownLocation != null) {
                fillViewsWithNewLocation(lastKnownLocation);
            }

            lastUpdateTimer = new Timer();

            TimerTask lastUpdateTimeTask = new TimerTask() {
                @Override
                public void run() {
                    getAndSetLastUpdatedTime();
                }
            };

            lastUpdateTimer.schedule(lastUpdateTimeTask, 0, 5000);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, locationListener);
        } catch (SecurityException e) {
            Log.e(MainActivity.class.getSimpleName(), "Error while starting activity", e);
        }
    }

    private void getAndSetLastUpdatedTime() {
        if(lastUpdatedTextView != null && lastKnownLocation!= null) {
            String v = "";

            long delta = (System.currentTimeMillis() - lastKnownLocation.getTime())/1000;

            if(delta <= 60) {
                if((int) (5*(Math.ceil(delta/5))) == 0) {
                    v = "0s";
                } else {
                    v = String.format("< " + String.valueOf((int) (5 * (Math.ceil((float)delta / 5)))) + "s");
                }
            } else if (delta > 60 && delta <= 60 * 60) {
                v = String.format("< " +  String.valueOf((int) (5*(Math.ceil((float)delta/(5*60*60))))) + "m");
            } else if (delta > 60 * 60 && delta <= 60 * 60 * 5) {
                v = String.format("< " +  String.valueOf((int) (Math.ceil((float)delta/(60*60)))) + "h");
            }  else if(delta > 60 * 60 * 5) {
                v = String.format("> 5h");
            }

            final String finalV = v;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lastUpdatedTextView.setText(finalV);
                }
            });
        }
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    private void treatNewLocation(Location location) {
        Log.i(MainActivity.class.getSimpleName(), "New location received" + location);
        if(isBetterLocation(location, lastKnownLocation)) {
            lastKnownLocation = location;

            fillViewsWithNewLocation(location);
        } else {
            Log.i(MainActivity.class.getSimpleName(), "Rejected new Location");
        }
    }

    private void fillViewsWithNewLocation(Location location) {
        locationSourceTextView.setText(getSourceName(location.getProvider()));
        locationAccuracyTextView.setText(String.format((int) location.getAccuracy() + " m"));
        getAndSetLastUpdatedTime();

        latitudeDDTextView.setText(String.format("%.8f", location.getLatitude()));
        longitudeDDTextView.setText(String.format("%.8f", location.getLongitude()));

        DMSCoordinates dms = DMSCoordinatesConverter.convertToDMS(location.getLatitude(), location.getLongitude());
        if(dms != null) {
            latitudeDMSTextView.setText(String.format(
                    dms.getLatD() + "° " +
                            dms.getLatM() + "' " +
                            String.format("%.3f", dms.getLatS()) + "\" " +
                            dms.getLatHemisphere()));

            longitudeDMSTextView.setText(String.format(
                    dms.getLongD() + "° " +
                            dms.getLongM() + "' " +
                            String.format("%.3f", dms.getLongS()) + "\" " +
                            dms.getLongHemisphere()));
        }

        UTMCoordinates utm = new UTMCoordinatesConverter().getUTMCoordinates(location.getLatitude(), location.getLongitude());
        if(utm != null) {
            zoneUTMTextView.setText(utm.getZone());
            latitudeUTMTextView.setText(String.format("%.2f", utm.getLatitude()));
            longitudeUTMTextView.setText(String.format("%.2f", utm.getLongitude()));
        }
    }

    private String getSourceName(String provider) {
        if(provider.equals(LocationManager.GPS_PROVIDER)) {
            return "GPS";
        } else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
            return "Network";
        } else {
            return provider;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > THIRTY_SECONDS;
        boolean isSignificantlyOlder = timeDelta < -THIRTY_SECONDS;
        boolean isNewer = timeDelta > 0;

        // If it's been more than n seconds since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}

package com.lionel.claudon.android.app.gpslocationviewer;

/**
 * Created by lionel on 03/11/15.
 */
public class DMSCoordinates {
    private String latHemisphere;
    private long latD;
    private long latM;
    private double latS;

    private String longHemisphere;
    private long longD;
    private long longM;
    private double longS;

    public DMSCoordinates(String latHemisphere, long latD, long latM, double latS, String longHemisphere, long longD, long longM, double longS) {
        this.latHemisphere = latHemisphere;
        this.latD = latD;
        this.latM = latM;
        this.latS = latS;
        this.longHemisphere = longHemisphere;
        this.longD = longD;
        this.longM = longM;
        this.longS = longS;
    }

    public String getLatHemisphere() {
        return latHemisphere;
    }

    public long getLatD() {
        return latD;
    }

    public long getLatM() {
        return latM;
    }

    public double getLatS() {
        return latS;
    }

    public String getLongHemisphere() {
        return longHemisphere;
    }

    public long getLongD() {
        return longD;
    }

    public long getLongM() {
        return longM;
    }

    public double getLongS() {
        return longS;
    }
}

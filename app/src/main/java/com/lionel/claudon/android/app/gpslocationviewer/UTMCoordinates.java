package com.lionel.claudon.android.app.gpslocationviewer;

/**
 * Created by lionel on 03/11/15.
 */
public class UTMCoordinates {
    private String zone;
    private double latitude;
    private double longitude;

    public UTMCoordinates(String zone, double latitude, double longitude) {
        this.zone = zone;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getZone() {
        return zone;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
